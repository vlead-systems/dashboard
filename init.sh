#!/bin/bash

if [ -d exp-publisher ]; then
    echo "experiment publisher already already present"
    (cd exp-publisher; git pull)
else
    git clone https://gitlab.com/vlead-projects/experiments/infra/publish/exp-publisher.git
    (cd exp-publisher)
fi

if [ -L pub-make ]; then
    echo "symlinked makefile already present"
else 
    ln -sf exp-publisher/makefile pub-make
fi

#!/bin/bash

git clone -b refactor https://gitlab.com/vlead-systems/cluster-bootstrap.git ./desktop
rsync -a ./desktop/src/realization-plan/milestones.org ./src/milestones/desktop.org
rm -rf ./desktop

git clone -b develop https://gitlab.com/vlead-systems/host-experiments.git  host-experiments
rsync -a ./host-experiments/src/realization-plan/milestones.org ./src/milestones/host-experiments.org
rm -rf host-experiments

git clone -b develop https://gitlab.com/vlead-systems/host-js-labs.git  host-js-labs
rsync -a ./host-js-labs/src/realization-plan/milestones.org ./src/milestones/host-js-labs.org
rm -rf host-js-labs


