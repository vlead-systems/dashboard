SHELL := /bin/bash

PWD=$(shell pwd)
STATUS=0

all: build

init:
	./init.sh

build: init
	make -f pub-make -k build

build-with-basic-infra: init clone-basic-infra build
	echo "build with basic infra"

clone-basic-infra: init wget-orgs clone-exporters clone-basic-themes
	echo "infrastructure put in place"

clone-basic-themes: init
	make -f pub-make -k clone-basic-themes

clone-exporters: init
	make -f pub-make -k clone-exporters

wget-orgs: init
	make -f pub-make -k wget-orgs

clean-milestones:
	(rm -rf src/milestones/desktop.org; \
	rm -rf src/milestones/host-experiments.org; \
	rm -rf src/milestones/host-js-labs.org)
clean: clean-milestones
	make -f pub-make clean
	(rm -rf build; \
        rm -rf ./exp-publisher; \
	rm -rf src/sitemap.org)

clean-infra:
	make -f pub-make clean-infra
