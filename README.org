* Dashboard for vlead systems
  The repo has to be built to view the dashboard.

* Steps to view the Dashboard
  1. Clone the repo and =cd= to the repository
     #+BEGIN_EXAMPLE
     git clone https://gitlab.com/vlead-systems/dashboard.git
     cd dashboard
     #+END_EXAMPLE
  2. Build the repo
     1. For the first time
        #+BEGIN_EXAMPLE
        make -k build-with-basic-infra
        #+END_EXAMPLE
     2. To build any time later
        #+BEGIN_EXAMPLE
        make -k all
        #+END_EXAMPLE
  3. View the Application
     #+BEGIN_EXAMPLE
     firefox build/docs/index.html
     #+END_EXAMPLE


